from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import IntegrityError
from django.shortcuts import render, get_object_or_404, redirect

from admin_site.forms import ShopOpenCloseForm, RepairShopUpdateForm
from admin_site.models import RepairShop, OpenClose

@login_required
def repair_shop_days(request):
    queryset=OpenClose.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(queryset, 10)

    try:
        queryset = paginator.page(page)

    except PageNotAnInteger:
        queryset = paginator.page(1)

    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)

    return render(request,"admin_site/repair_shop/repair_shop_days.html",{'objects':queryset})

@login_required
def timing_create(request):
    if request.method=='POST':
        form = ShopOpenCloseForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect("admin_site:repair_shop_days")
    else:
        form = ShopOpenCloseForm()

    return render(request,"admin_site/repair_shop/timing_create.html",{'form':form})

@login_required
def timing_update(request,id):
    timing=get_object_or_404(OpenClose,id=id)
    form=ShopOpenCloseForm(request.POST or None, instance=timing)

    if form.is_valid():
        form.save()
        return redirect('admin_site:repair_shop_days')

    return render(request,"admin_site/repair_shop/timing_create.html",{'form':form})

@login_required
def timing_delete(request,id):
    timing=get_object_or_404(OpenClose,id=id)
    timing.delete()
    return redirect("admin_site:repair_shop_days")

@login_required
def repair_shop_update(request):
    shop=RepairShop.objects.first()
    form = RepairShopUpdateForm(request.POST or None, instance=shop)

    if form.is_valid():
        form.save()
        return redirect('admin_site:repair_shop_update')

    return render(request, "admin_site/repair_shop/repair_shop_update.html", {'form': form})