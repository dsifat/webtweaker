from django.apps import AppConfig


class AdminSiteConfig(AppConfig):
    name = 'src'
    verbose_name = 'Webtweaker Admin Panel'
