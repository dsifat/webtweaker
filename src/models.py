from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.core.validators import MaxValueValidator, MinLengthValidator, MinValueValidator, RegexValidator
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from notifications.signals import notify

class ServiceType(models.Model):
    name=models.CharField(max_length=200,null=False,blank=False)
    description=models.CharField(max_length=1000)

    def __str__(self):
        return self.name

class ServiceOrder(models.Model):

    name=models.ForeignKey(ServiceType,on_delete=models.CASCADE)
    completed=models.BooleanField(default=False)
    client_name=models.CharField(max_length=200,blank=False,null=False)
    client_mail=models.EmailField(max_length=30,blank=False,null=False)

    phone_regex=RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    client_phone=models.CharField(max_length=15,blank=False,null=False,validators=[phone_regex])

    car_year=models.IntegerField(validators=[MaxValueValidator(2018),
            MinValueValidator(1980)],null=True,blank=True)
    car_make=models.CharField(max_length=100)
    car_model=models.CharField(max_length=255)

    appointment_type=models.CharField(max_length=100, blank=False, null=False)
    appointment_date=models.DateField()
    message=models.TextField()
    created=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

@receiver(post_save,sender=ServiceOrder)
def appointment_notification(sender,instance,created,**kwargs):
    users=User.objects.all()
    notify.send(instance, verb='A client requested for your appointment and appointment ID is: ',recipient=users)
    send_email(instance)

def send_email(instance):
    subject = str(instance.appointment_type)
    client_name = "Client Name: "+ str(instance.client_name)
    client_mail = "Client Email: "+ str(instance.client_mail)
    client_phone = "Client Phone: "+ str(instance.client_phone)
    car_year = "Car Year: " + str(instance.car_year)
    car_make = "Car Make: "+ str(instance.car_make)
    car_model = "Car Model:"+ str(instance.car_model)
    appointment_date = "Appointment Date: "+ str(instance.appointment_date)
    message = "Message: "+ str(instance.message)
    created = "Created: "+ str(instance.created)
    email = EmailMessage(subject=subject, to=['support@kathysautorepairshop.com'], body=client_name+"\n"+client_mail+"\n"+client_phone+"\n"+car_year+"\n"+car_make+"\n"+car_model+"\n"+appointment_date+"\n"+message+"\n"+created,
                         headers={'Content-Type': 'text/plain'}, reply_to=[instance.client_mail])
    email.send()

class ContentManager(models.Model):
    pass

class Review(models.Model):
    name = models.CharField(max_length=40,blank=False,null=False)
    city = models.CharField(max_length=40,blank=True,null=True)
    message = models.TextField(max_length=180,blank=False,null=False)
    created = models.DateField(auto_now_add=True)