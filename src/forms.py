from django import forms
from django.forms import Select, Textarea, TextInput, EmailInput, ModelForm, DateField, NumberInput

from src.models import Review
from src.choices import type_order
from src.models import ServiceOrder


class ServiceOrderForm(ModelForm):
    class Meta:
        model=ServiceOrder
        fields=['client_name','client_phone','client_mail','car_year','car_make','car_model' ,'appointment_date','name','message','completed']
        widgets={
            'client_name':TextInput(attrs={'class':'form-control'}),
            'client_phone':EmailInput(attrs={'class':'form-control'}),
            'client_mail':TextInput(attrs={'class':'form-control'}),
            'car_year':TextInput(attrs={'class':'form-control'}),
            'car_make':TextInput(attrs={'class':'form-control'}),
            'car_model':TextInput(attrs={'class':'form-control'}),
            'appointment_date':TextInput(attrs={'class':'form-control'}),
            'name':Select(attrs={'class':'mdb-select'}),
            'message': Textarea(attrs={'class': 'form-control'}),
        }
        labels = {
            'client_name':'Client Name',
            'client_phone':'Client Phone',
            'client_mail':'Client Mail',
            'car_year':'Car Year',
            'car_make':'Car Make',
            'car_model':'Car Model',
            'appointment_date':'Date',
            'name': 'Select Service'
        }

class AppointmentForm(ModelForm):
    appointment_type=forms.ChoiceField(choices=type_order,widget=forms.Select(attrs={'class':'field select2'}))
    class Meta:
        model=ServiceOrder
        fields=['appointment_type','client_name','client_phone','client_mail','car_year','car_make','car_model' ,'appointment_date','name','message']
        widgets={
            'client_name':TextInput(attrs={'class':'field'}),
            'client_phone':TextInput(attrs={'class':'field'}),
            'client_mail':EmailInput(attrs={'class':'field'}),
            'car_year':NumberInput(attrs={'class':'field'}),
            'car_make':TextInput(attrs={'class':'field'}),
            'car_model':TextInput(attrs={'class':'field'}),
            'appointment_type':Select(attrs={'class':'field select2'}),
            'appointment_date':TextInput(attrs={'class':'field cm-date','data-displayname':'Date','readonly':''}),
            'name':Select(attrs={'class':'field select2'}),
            'message': Textarea(attrs={'class': 'field'}),
        }
        labels = {
            'client_name':'Name',
            'client_phone':'Phone',
            'client_mail':'Mail',
            'car_year':'Car Year',
            'car_make':'Car Make',
            'car_model':'Car Model',
            'appointment_type':'Service Type',
            'appointment_date':'Date',
            'name': 'Select Service',
            'message':'Message',
        }

class ReviewForm(ModelForm):
    class Meta:
        model=Review
        fields=['name','city','message']
        widgets={
            'name':TextInput(attrs={'class':'field'}),
            'city':TextInput(attrs={'class':'field'}),
            'message':Textarea(attrs={'class':'field'}),
        }